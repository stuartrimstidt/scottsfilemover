from shutil import copy2
import os
from datetime import datetime
import time
import re

fileGroups = []

# Define Configs here
movies = {
    "srcDir": './tmp',
    "destDir": './tmp2',
    "fileMoveThresholdDays": 2
}

tv = {
    "srcDir": './tmp3',
    "destDir": './tmp4',
    "fileMoveThresholdDays": 2
}

# Add to run list here
fileGroups.append(movies)
fileGroups.append(tv)

# Get current timestamp for log file name
logTimestamp = datetime.now().strftime("%m.%d.%Y_%H.%M.%S")

# Create log file name based on timestamp
logFilePath = './logs/' + logTimestamp + '_log.txt'

# Open file stream - create the file in write mode
logFile = open(logFilePath, "w")


def logWrite(printStr):
    ts = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
    logStr = ts + ' | ' + printStr
    print(logStr)
    logFile.write(logStr + '\n')


def getListOfFiles(dirName, dirConst):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath, dirConst)
        else:
            relPath = fullPath.split(dirConst, 1)
            allFiles.append(relPath[1])

    return allFiles


# Cleanup old log files
for f in os.listdir('./logs'):
    creation_time = os.path.getctime('./logs/' + f)
    if (time.time() - creation_time) // (24 * 3600) >= 14:
        os.unlink('./logs/' + f)
        logWrite('Deleted old log: ' + f)

for fileGroup in fileGroups:
    logWrite('Begin New File Group')

    # Set global variables from config dictionary
    gblSrcDir = fileGroup['srcDir']
    gblDestDir = fileGroup['destDir']
    fileMoveThresholdDays = fileGroup['fileMoveThresholdDays']

    # Run recursive function to get the list of files
    fileList = getListOfFiles(gblSrcDir, gblSrcDir)

    # Loop through the files
    for file in fileList:
        try:
            # Get the modified time of the file
            modTime = datetime.fromtimestamp(
                os.path.getctime(gblSrcDir + file))

            # Get the time delta in days since it was last modified
            timeSinceMod = datetime.now() - modTime

            # If the day delta is greater than the threshold
            # that's when we copy the file
            # Change this shit to >
            if timeSinceMod.days < fileMoveThresholdDays:
                srcFile = gblSrcDir + file
                destFile = gblDestDir + file

                # If the destination file exists, don't copy again
                # Continue to next file iteration
                if os.path.exists(destFile):
                    # If the files are the same size, skip it
                    if os.stat(destFile).st_size == os.stat(srcFile).st_size:
                        logWrite('File exists. Skipping file: ' + srcFile)
                        continue

                # Regular expression to separate the filename from the path
                # Don't worry about understanding this - regex is by far
                # the most difficult thing to grasp
                regEx = re.split(r"^(.+)\/([^/]+)$", file)

                # If the relative directory doesn't exist, create them
                # so the copy doesn't fail
                if len(regEx) > 1:
                    relDirectory = regEx[1]

                    if not os.path.exists(gblDestDir + relDirectory):
                        os.makedirs(gblDestDir + relDirectory)

                logWrite('Copying file: ' + srcFile)
                logWrite('To: ' + destFile)

                # All of that shit and here's the part that actually
                # copies it
                copy2(srcFile, destFile)

                logWrite('Copy Complete: ' + destFile)
        except:
            logWrite('Uh oh! Something failed')

    logWrite('File Group Complete')
    logWrite('--------------------------------------------')

# Close file stream
logFile.close()
